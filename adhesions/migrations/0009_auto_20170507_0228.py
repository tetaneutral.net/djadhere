# Generated by Django 1.11 on 2017-05-07 00:28

from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ("adhesions", "0008_auto_20170415_2307"),
    ]

    operations = [
        migrations.AddField(
            model_name="corporation",
            name="email",
            field=models.EmailField(
                blank=True, max_length=254, verbose_name="Adresse e-mail"
            ),
        ),
        migrations.AddField(
            model_name="corporation",
            name="notes",
            field=models.TextField(blank=True, default=""),
        ),
        migrations.AddField(
            model_name="corporation",
            name="phone_number",
            field=models.CharField(
                blank=True,
                default="",
                max_length=16,
                verbose_name="Numéro de téléphone",
            ),
        ),
    ]
