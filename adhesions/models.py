from datetime import timedelta

from django.contrib.auth.models import User as AuthUser
from django.contrib.auth.models import UserManager
from django.contrib.humanize.templatetags.humanize import naturalday
from django.db import models
from django.urls import reverse
from django.utils import timezone
from django.utils.html import format_html

import requests

from banking.models import Echeance, PaymentUpdate, RecurringPayment
from services.models import Service


class ActiveUserManager(UserManager):
    def get_queryset(self):
        qs = super().get_queryset()
        return qs.annotate(
            adhesion_long_stopped=~models.Exists(
                Adhesion.objects.filter(user=models.OuterRef("pk")).filter(
                    long_stopped=False,
                ),
            ),
            long_stopped=models.Case(
                models.When(adhesion_long_stopped=True, then=True),
                default=False,
                output_field=models.BooleanField(),
            ),
        )


class ActiveAdhesionManager(models.Manager):
    def get_queryset(self):
        qs = super().get_queryset()
        one_year_ago = timezone.now() - timedelta(days=366)
        return qs.annotate(
            last_echeance=models.Subquery(
                Echeance.objects.filter(adhesion=models.OuterRef("pk")).values("date")[
                    :1
                ],
            ),
            payment_method=models.Subquery(
                PaymentUpdate.objects.filter(
                    payment=models.OuterRef("membership__pk"),
                ).values("payment_method")[:1],
            ),
            payment_start=models.Subquery(
                PaymentUpdate.objects.filter(
                    payment=models.OuterRef("membership__pk"),
                ).values("start")[:1],
            ),
            active=models.Case(
                models.When(payment_method__isnull=True, then=None),
                models.When(payment_method=PaymentUpdate.STOP, then=False),
                default=True,
                output_field=models.BooleanField(null=True),
            ),
            services_long_stopped=~models.Exists(
                Service.objects.filter(adhesion=models.OuterRef("pk")).filter(
                    long_stopped=False,
                ),
            ),
            long_stopped=models.Case(
                models.When(services_long_stopped=False, then=False),
                models.When(
                    payment_method=PaymentUpdate.STOP,
                    payment_start__lte=one_year_ago,
                    then=True,
                ),
                default=False,
                output_field=models.BooleanField(),
            ),
        )


class User(AuthUser):
    objects = ActiveUserManager()

    def get_model_perms(self, request):
        return {}

    @property
    def adhesions(self):
        """user and corporations (for which the user is a member) adhesions"""
        return self.profile.adhesions

    @property
    def phone_number(self):
        return self.profile.phone_number

    @property
    def address(self):
        return self.profile.address

    class Meta:
        proxy = True
        verbose_name = "personne physique"
        verbose_name_plural = "personnes physiques"

    def get_absolute_url(self):
        return reverse(
            f"admin:{self._meta.app_label}_{self._meta.model_name}_change",
            args=(self.pk,),
        )

    def __str__(self):
        return self.get_full_name() or self.username


class Corporation(models.Model):
    social_reason = models.CharField(
        max_length=256,
        verbose_name="Raison sociale",
        unique=True,
    )
    description = models.TextField(blank=True, default="")
    members = models.ManyToManyField(
        User,
        blank=True,
        verbose_name="Membres",
        related_name="corporations",
        related_query_name="corporation",
    )
    email = models.EmailField(verbose_name="Adresse e-mail", blank=True)
    phone_number = models.CharField(
        max_length=16,
        blank=True,
        default="",
        verbose_name="Numéro de téléphone",
    )
    address = models.TextField(blank=True, default="", verbose_name="Adresse")
    notes = models.TextField(blank=True, default="")

    class Meta:
        verbose_name = "personne morale"
        verbose_name_plural = "personnes morales"

    def get_absolute_url(self):
        return reverse(
            f"admin:{self._meta.app_label}_{self._meta.model_name}_change",
            args=(self.pk,),
        )

    def __str__(self):
        return self.social_reason


class Adhesion(models.Model):
    """
    Terminologie : une « adhésion » désgine une instance de ce modèle
    tandis qu`un·e « adhérent·e » désigne un·e user ou une corporation.
    """

    limit = models.Q(app_label="auth", model="user") | models.Q(
        app_label="adhesions",
        model="corporation",
    )
    id = models.AutoField(
        verbose_name="Numéro d`adhérent·e",
        primary_key=True,
        editable=True,
    )
    created = models.DateTimeField(null=True, blank=True, auto_now_add=True)
    notes = models.TextField(
        blank=True,
        default="",
        verbose_name="Notes (obsolète)",
        help_text="Obsolète, ne plus ajouter d`informations ici.",
    )
    active_legacy = models.BooleanField(
        default=None,
        verbose_name="Adhésion en cours",
        null=True,
    )

    user = models.OneToOneField(User, null=True, on_delete=models.PROTECT)
    corporation = models.OneToOneField(Corporation, null=True, on_delete=models.PROTECT)

    membership = models.OneToOneField(RecurringPayment, on_delete=models.CASCADE)

    objects = ActiveAdhesionManager()

    def save(self, *args, **kwargs):
        if not hasattr(self, "membership"):
            self.membership = RecurringPayment.objects.create()
        super().save(*args, **kwargs)

    class Meta:
        verbose_name = "adhésion"
        ordering = ("id",)

    def is_physical(self):
        return self.user is not None

    def is_moral(self):
        return self.corporation is not None

    @property
    def type(self):
        return "Personne physique" if self.is_physical() else "Personne morale"

    @property
    def adherent(self):
        return self.user if self.is_physical() else self.corporation

    def get_adherent_link(self):
        return format_html(
            '<a href="{}">{}</a>',
            self.adherent.get_absolute_url(),
            self.adherent,
        )

    get_adherent_link.short_description = "Nom ou raison sociale"

    def get_adhesion_link(self):
        return format_html(
            '<a href="{}">ADT{}</a>',
            self.get_absolute_url(),
            str(self.id),
        )

    get_adhesion_link.short_description = "Numéro d`adhérent·e"

    def get_absolute_url(self):
        return reverse(
            f"admin:{self._meta.app_label}_{self._meta.model_name}_change",
            args=(self.pk,),
        )

    def last_echeance_display(self):
        if self.last_echeance:
            return naturalday(self.last_echeance)
        return None

    def get_ssh_config(self):
        if self.is_physical():
            return self.user.profile.ssh_keys
        return "\n".join(
            self.corporation.members.values_list("profile__ssh_keys", flat=True),
        )

    def get_ssh_keys(self):
        keys = set()
        for line in self.get_ssh_config().splitlines():
            if line.startswith("https://"):
                try:
                    r = requests.get(line)
                    r.raise_for_status()
                    keys.update(r.content.decode().splitlines())
                except (requests.ConnectionError, requests.HTTPError) as err:
                    keys.add(f"# error retrieving {line}: {err}")
            else:
                keys.add(line)
        return "\n".join(sorted(keys))

    last_echeance_display.short_description = "Dernier prélèvement"
    last_echeance_display.admin_order_field = "last_echeance"

    def is_active(self):
        return self.active

    is_active.boolean = True
    is_active.short_description = "Actif"

    def __str__(self):
        # return "ADT%d" % self.id
        if self.id is None:
            return "?"
        return "ADT%d (%s)" % (self.id, self.adherent)
