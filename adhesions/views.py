from django.contrib.auth.decorators import login_required
from django.core.exceptions import PermissionDenied
from django.shortcuts import get_object_or_404, render

from .models import Corporation, User


@login_required
def user(request):
    # request.user is concrete model whereas user is proxy model
    user = User.objects.get(pk=request.user.pk)
    return render(
        request,
        "adhesions/user.html",
        {
            "adherent": user,
            "adhesion": user.adhesion,
        },
    )


@login_required
def corporation(request, pk):
    corporation = get_object_or_404(Corporation, pk=pk)
    if not request.user.is_superuser:
        try:
            corporation.members.get(pk=request.user.pk)
        except User.DoesNotExist:
            raise PermissionDenied from None
    return render(
        request,
        "adhesions/corporation.html",
        {
            "adherent": corporation,
            "adhesion": corporation.adhesion,
        },
    )
