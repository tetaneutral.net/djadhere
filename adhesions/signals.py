from django.db.models.signals import post_delete, post_save
from django.dispatch import receiver

from .models import Adhesion, Corporation, User


@receiver(post_save, sender=User, dispatch_uid="create_user_adhesion")
def create_user_adhesion(sender, instance, created, **kwargs):
    if created and not hasattr(instance, "skip_adhesion_creation"):
        Adhesion.objects.create(user=instance)


@receiver(post_save, sender=Corporation, dispatch_uid="create_corporation_adhesion")
def create_corporation_adhesion(sender, instance, created, **kwargs):
    if created and not hasattr(instance, "skip_adhesion_creation"):
        Adhesion.objects.create(corporation=instance)


@receiver(post_delete, sender=Adhesion, dispatch_uid="delete_adhesion")
def delete_adhesion(sender, instance, **kwargs):
    if instance.is_physical():
        instance.user.delete()
    if instance.is_moral():
        instance.corporation.delete()
    instance.membership.delete()
