from django.apps import AppConfig


class BankingConfig(AppConfig):
    name = "banking"
    verbose_name = "Comptabilité"

    def ready(self):
        import banking.checks  # noqa
