from django.conf import settings
from django.core.checks import Error, register


@register()
def check_settings(app_configs, **kwargs):
    errors = []
    if not hasattr(settings, "PAYMENTS_EMAILS"):
        errors.append(Error("Missing settings variable PAYMENTS_EMAILS."))
    return errors
