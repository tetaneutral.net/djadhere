from operator import add

from django.core.management.base import BaseCommand
from django.db.models import Q

from adhesions.models import Adhesion
from banking.models import PaymentUpdate, RecurringPayment
from djadhere.utils import get_active_filter
from services.models import ServiceType


class Command(BaseCommand):
    help = "Afficher les statistiques financières"

    def add_arguments(self, parser):
        parser.add_argument("--services", action="store_true")
        parser.add_argument("--adhesions", action="store_true")
        parser.add_argument("--methodes", action="store_true")

    def handle(self, *args, **options):
        if options["services"]:
            self.handle_services()
        if options["adhesions"]:
            self.handle_adhesions()
        if options["methodes"]:
            self.handle_methodes()

    def handle_services(self):
        ttnn = Adhesion.objects.get(id=100)
        total = [0, 0, 0, 0, 0, 0, 0]
        lines = []
        for service_type in ServiceType.objects.exclude(name="Contribution"):
            ntotal = ninf = nadh = npay = nfree = nmiss = income = 0
            for service in (
                service_type.services.filter(get_active_filter("allocation"))
                .order_by("pk")
                .distinct()
            ):
                ntotal += 1
                if service.adhesion == ttnn:
                    ninf += 1
                    continue
                nadh += 1
                contrib = service.contribution.get_current_payment()
                if not contrib or contrib.payment_method == PaymentUpdate.STOP:
                    nmiss += 1
                elif contrib.payment_method == PaymentUpdate.FREE:
                    nfree += 1
                else:
                    npay += 1
                    income += float(contrib.amount) / contrib.period
            assert ntotal == ninf + nadh
            assert nadh == npay + nfree + nmiss
            total = list(
                map(add, total, [ntotal, ninf, nadh, npay, nfree, nmiss, income]),
            )
            lines += [
                (str(service_type), ntotal, ninf, nadh, npay, nfree, nmiss, income),
            ]
        self.stdout.write(
            "%-18s%12s%12s%14s%12s%12s%12s%12s%12s%12s"
            % (
                "Service",
                "total",
                "infra",
                "adhérent·e·s",
                "gratuits",
                "manquants",
                "payés",
                "euros",
                "moyenne",
                "pourcent",
            ),
        )
        lines += [("TOTAL", *tuple(total))]
        total_income = total[6]
        for service_type, ntotal, ninf, nadh, npay, nfree, nmiss, income in lines:
            if total_income:
                percent = income / total_income * 100
            else:
                percent = 0
            if npay:
                moy = "%12.2f" % (income / npay)
            else:
                moy = "%12s" % "-"
            self.stdout.write(
                "%-18s%12d%12d%14d%12d%12d%12d%12.2f%s%12.1f"
                % (
                    service_type,
                    ntotal,
                    ninf,
                    nadh,
                    nfree,
                    nmiss,
                    npay,
                    income,
                    moy,
                    percent,
                ),
            )

    def handle_adhesions(self):
        adhesions = Adhesion.objects.filter(Q(active__isnull=True) | Q(active=True))
        nadh = adhesions.count()
        pmiss, pgra, ppay, income = 0, 0, 0, 0
        payments = (adh.membership.get_current_payment() for adh in adhesions)
        for payment in payments:
            if payment is None:
                pmiss += 1
            elif payment.payment_method == PaymentUpdate.FREE:
                pgra += 1
            else:
                assert payment.payment_method != PaymentUpdate.STOP
                ppay += 1
                income += float(payment.amount) / payment.period
        self.stdout.write(
            "%12s%12s%12s%12s%12s%12s"
            % ("Adhesions", "gratuites", "manquantes", "payées", "euros", "moyenne"),
        )
        self.stdout.write(
            "%12d%12d%12d%12d%12.2f%12.2f"
            % (nadh, pgra, pmiss, ppay, income, 12 * income / ppay),
        )

    def handle_methodes(self):
        prelevement, virement, facture = 0, 0, 0
        for payment in RecurringPayment.objects.all():
            payment = payment.get_current_payment()
            if not payment:
                continue
            if payment.payment_method == PaymentUpdate.DEBIT:
                prelevement += payment.amount / payment.period
            elif payment.payment_method == PaymentUpdate.TRANSFER:
                virement += payment.amount / payment.period
            elif payment.payment_method == PaymentUpdate.INVOICE:
                facture += payment.amount / payment.period
        self.stdout.write(
            "%12s%12s%12s%12s" % ("prélèvement", "virement", "facture", "total"),
        )
        self.stdout.write(
            f"{prelevement:12.2f}{virement:12.2f}{facture:12.2f}"
            f"{prelevement + virement + facture:12.2f}",
        )
