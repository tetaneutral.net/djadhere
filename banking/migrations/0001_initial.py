# Generated by Django 1.10.4 on 2016-12-30 02:58

import django.core.validators
from django.db import migrations, models


class Migration(migrations.Migration):
    initial = True

    dependencies = []

    operations = [
        migrations.CreateModel(
            name="Payment",
            fields=[
                (
                    "id",
                    models.AutoField(
                        auto_created=True,
                        primary_key=True,
                        serialize=False,
                        verbose_name="ID",
                    ),
                ),
                (
                    "amount",
                    models.DecimalField(
                        decimal_places=2, max_digits=9, verbose_name="Montant"
                    ),
                ),
                (
                    "period",
                    models.PositiveIntegerField(
                        validators=[django.core.validators.MaxValueValidator(12)],
                        verbose_name="Période",
                    ),
                ),
                (
                    "payment_method",
                    models.IntegerField(
                        choices=[(0, "Virement"), (1, "Prélèvement")],
                        verbose_name="Méthode de paiement",
                    ),
                ),
            ],
            options={
                "verbose_name": "paiement",
            },
        ),
    ]
