{
  description = "tetaneutral.net SI";

  inputs = {
    flake-utils.url = "github:numtide/flake-utils";
    nixpkgs.url = "github:NixOS/nixpkgs/nixos-unstable";
    poetry2nix = {
      url = "github:nix-community/poetry2nix";
      inputs.nixpkgs.follows = "nixpkgs";
    };
  };

  outputs =
    {
      nixpkgs,
      flake-utils,
      poetry2nix,
      ...
    }:
    flake-utils.lib.eachDefaultSystem (
      system:
      let
        # see https://github.com/nix-community/poetry2nix/tree/master#api for more functions and examples.
        pkgs = nixpkgs.legacyPackages.${system};
        inherit (poetry2nix.lib.mkPoetry2Nix { inherit pkgs; }) mkPoetryApplication defaultPoetryOverrides;
        djadhere = mkPoetryApplication {
          projectDir = ./.;
          preferWheels = true; # TODO: not clean, but fine as long as it is not used in production
          overrides = defaultPoetryOverrides.extend (
            _: _: {
              inherit (pkgs.python311Packages) reportlab; # TODO: even the wheel is not working for that one ? :(
            }
          );
        };
      in
      {
        packages.default = djadhere;
        devShells.default = pkgs.mkShell {
          inputsFrom = [ djadhere ];
          packages = [
            pkgs.gdal
            pkgs.geos
            pkgs.postgresql
            (pkgs.poetry.withPlugins (ps: [ ps.poetry-plugin-up ]))
          ];
          shellHook = ''
            export UID=$(id -u)
            export GID=$(id -g)
            export LD_LIBRARY_PATH="${pkgs.gdal}/lib"
          '';
        };
      }
    );
}
