class PonyAirRouter:
    def db_for_read(self, model, **hints):
        if model._meta.app_label == "ponyair":
            return "ponyair"
        return None

    def db_for_write(self, model, **hints):
        if model._meta.app_label == "ponyair":
            return "ponyair"
        return None

    def allow_relation(self, obj1, obj2, **hints):
        return (
            obj1._meta.app_label == "ponyair" and obj2._meta.app_label == "ponyair"
        ) or (obj1._meta.app_label != "ponyair" and obj2._meta.app_label != "ponyair")

    def allow_migrate(self, db, app_label, model_name=None, **hints):
        return (db == "ponyair" and app_label == "ponyair") or (
            db != "ponyair" and app_label != "ponyair"
        )
