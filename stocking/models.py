from django.db import models

from mptt.models import MPTTModel, TreeForeignKey


class Category(MPTTModel):
    parent = TreeForeignKey(
        "self",
        null=True,
        blank=True,
        related_name="children",
        db_index=True,
        verbose_name="catégorie parente",
        on_delete=models.CASCADE,
    )
    name = models.CharField(max_length=64, verbose_name="nom")

    class MPTTMeta:
        order_insertion_by = ("name",)

    class Meta:
        verbose_name = "Catégorie"

    def __str__(self):
        return self.name


class Location(models.Model):
    name = models.CharField(max_length=64, verbose_name="nom")

    class Meta:
        verbose_name = "Emplacement"

    def __str__(self):
        return self.name


class Equipment(models.Model):
    category = TreeForeignKey(
        Category,
        verbose_name="catégorie",
        related_name="equipments",
        on_delete=models.CASCADE,
    )
    quantity = models.PositiveIntegerField(default=0, verbose_name="quantité")
    location = models.ForeignKey(
        Location,
        verbose_name="emplacement",
        related_name="equipments",
        on_delete=models.CASCADE,
    )

    class Meta:
        verbose_name = "Équipement"
        unique_together = (
            (
                "category",
                "location",
            ),
        )

    def __str__(self):
        return "%d %s chez %s" % (self.quantity, self.category, self.location)
