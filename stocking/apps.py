from django.apps import AppConfig


class StockingConfig(AppConfig):
    name = "stocking"
    verbose_name = "Inventaire"
