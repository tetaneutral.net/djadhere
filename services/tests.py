from datetime import datetime, timedelta
from io import StringIO

from django.core.management import call_command
from django.core.management.base import CommandError
from django.db.models import Q
from django.test import TestCase
from django.urls import reverse
from django.utils import timezone

from adhesions.models import Corporation, User
from banking.models import PaymentUpdate, RecurringPayment

from .models import IPResource, Route, Service, ServiceAllocation, ServiceType


class ServicesMixin:
    def setUp(self):
        user1 = User.objects.create_user(
            "user1",
            email="user1@example.net",
            password="user1",
        )
        corp1 = Corporation.objects.create(social_reason="Corp 1")
        corp1.members.add(user1)
        user2 = User.objects.create_user(
            "user2",
            email="user2@example.net",
            password="user2",
            is_superuser=True,
            is_staff=True,
        )
        corp2 = Corporation.objects.create(social_reason="Corp 2")
        corp2.members.add(user2)
        stype1 = ServiceType.objects.create(name="VM")
        stype2 = ServiceType.objects.create(name="Abo Toulouse")
        ServiceType.objects.create(name="Abo Castre")
        s1 = Service.objects.create(
            adhesion=user1.adhesion,
            service_type=stype1,
            label="Service 1",
        )
        Service.objects.create(
            adhesion=user2.adhesion,
            service_type=stype1,
            label="Service 2",
        )
        Service.objects.create(
            adhesion=corp1.adhesion,
            service_type=stype2,
            label="Service 3",
        )
        Service.objects.create(
            adhesion=corp2.adhesion,
            service_type=stype1,
            label="Service 4",
            # active=False,
        )
        ip1 = IPResource.objects.create(
            ip="91.224.148.1",
            category=IPResource.CATEGORY_PUBLIC,
        )
        route1 = Route.objects.create(name="route1")
        ServiceAllocation.objects.create(
            resource=ip1,
            service=s1,
            route=route1,
            start=timezone.now(),
        )
        IPResource.objects.create(
            ip="91.224.148.2",
            category=IPResource.CATEGORY_PUBLIC,
        )
        IPResource.objects.create(
            ip="91.224.148.3",
            category=IPResource.CATEGORY_PUBLIC,
        )


class AdminTestCase(ServicesMixin, TestCase):
    def test_admin_lists(self):
        self.client.login(username="user2", password="user2")
        for model in [
            "ipresource",
            "route",
            "ipprefix",
            "service",
            "switch",
            "tunnel",
            "servicetype",
        ]:
            response = self.client.get(reverse(f"admin:services_{model}_changelist"))
            self.assertEqual(response.status_code, 200)

    def test_admin_change(self):
        self.client.login(username="user2", password="user2")

        def get_first(model):
            return self.client.get(
                reverse(
                    f"admin:services_{model._meta.model_name}_change",
                    args=[model.objects.first().pk],
                ),
            )

        self.assertEqual(get_first(IPResource).status_code, 200)
        self.assertEqual(get_first(Route).status_code, 200)
        self.assertEqual(get_first(Service).status_code, 200)
        self.assertEqual(get_first(ServiceType).status_code, 200)


class ViewsTestCase(ServicesMixin, TestCase):
    def test_home_service_list(self):
        self.client.login(username="user1", password="user1")
        response = self.client.get(reverse("adhesion-detail"))
        self.assertContains(response, "Service 1")
        self.assertNotContains(response, "Service 2")
        self.assertNotContains(response, "Service 3")
        self.assertNotContains(response, "Service 4")

    def test_service_detail(self):
        s1 = Service.objects.get(label="Service 1")
        s2 = Service.objects.get(label="Service 2")
        s3 = Service.objects.get(label="Service 3")
        s4 = Service.objects.get(label="Service 4")
        # L`utilisateur lambda n`a accès qu`aux services 1 et 3
        self.client.login(username="user1", password="user1")
        response = self.client.get(reverse("service-detail", kwargs={"pk": s1.pk}))
        self.assertEqual(response.status_code, 200)
        response = self.client.get(reverse("service-detail", kwargs={"pk": s2.pk}))
        self.assertEqual(response.status_code, 404)
        response = self.client.get(reverse("service-detail", kwargs={"pk": s3.pk}))
        self.assertEqual(response.status_code, 200)
        response = self.client.get(reverse("service-detail", kwargs={"pk": s4.pk}))
        self.assertEqual(response.status_code, 404)
        # L`admin a accès à tous les services
        self.client.login(username="user2", password="user2")
        response = self.client.get(reverse("service-detail", kwargs={"pk": s1.pk}))
        self.assertEqual(response.status_code, 200)
        response = self.client.get(reverse("service-detail", kwargs={"pk": s2.pk}))
        self.assertEqual(response.status_code, 200)
        response = self.client.get(reverse("service-detail", kwargs={"pk": s3.pk}))
        self.assertEqual(response.status_code, 200)
        response = self.client.get(reverse("service-detail", kwargs={"pk": s4.pk}))
        self.assertEqual(response.status_code, 200)


class ServicesCommandTestCase(ServicesMixin, TestCase):
    # def test_stats(self):
    # TODO: commande trop vieille. je sais pas l`update.
    # out = StringIO()
    # call_command("services", "stats", stdout=out)
    # result = out.getvalue()
    # self.assertRegex(result, "VM")
    # self.assertRegex(result, "Abo Toulouse")

    def test_list(self):
        out = StringIO()
        call_command("services", "list", stdout=out)
        result = out.getvalue()
        self.assertRegex(result, "Service 1")
        self.assertRegex(result, "Service 4")

        out = StringIO()
        call_command("services", "list", "--active", stdout=out)
        result = out.getvalue()
        self.assertRegex(result, "Service 1")
        self.assertNotRegex(result, "Service 4")

        out = StringIO()
        call_command("services", "list", "--inactive", stdout=out)
        result = out.getvalue()
        self.assertNotRegex(result, "Service 1")
        self.assertRegex(result, "Service 4")

        out = StringIO()
        call_command("services", "list", "--type", "VM", stdout=out)
        result = out.getvalue()
        self.assertRegex(result, "Service 1")
        self.assertNotRegex(result, "Service 3")

    def test_show(self):
        pass

    def test_add(self):
        pass

    def test_delete(self):
        pass


class ServiceTypesCommandTestCase(ServicesMixin, TestCase):
    def test_list(self):
        out = StringIO()
        call_command("servicetypes", "list", stdout=out)
        result = out.getvalue()
        self.assertRegex(result, "VM")

    def test_add(self):
        self.assertRaisesRegex(
            CommandError,
            "type de service .* existe déjà",
            call_command,
            "servicetypes",
            "add",
            "VM",
            stdout=None,
        )
        out = StringIO()
        call_command("servicetypes", "add", "Abo Muret", stdout=out)
        result = out.getvalue()
        self.assertRegex(result, "succès")
        ServiceType.objects.get(name="Abo Muret")

    def test_change(self):
        st_old = ServiceType.objects.get(name="Abo Toulouse")
        self.assertRaisesRegex(
            CommandError,
            "type de service .* existe déjà",
            call_command,
            "servicetypes",
            "change",
            "Abo Toulouse",
            "VM",
            stdout=None,
        )
        self.assertRaisesRegex(
            CommandError,
            "type de service .* n`existe pas",
            call_command,
            "servicetypes",
            "change",
            "Abo Muret",
            "Abo Auch",
            stdout=None,
        )
        out = StringIO()
        call_command("servicetypes", "change", "Abo Toulouse", "Abo Muret", stdout=out)
        result = out.getvalue()
        self.assertRegex(result, "renommé")
        st_new = ServiceType.objects.get(name="Abo Muret")
        self.assertEqual(st_old.pk, st_new.pk)

    def test_delete(self):
        self.assertRaisesRegex(
            CommandError,
            "suppression refusé",
            call_command,
            "servicetypes",
            "delete",
            "VM",
            stdout=None,
        )
        self.assertRaisesRegex(
            CommandError,
            "type de service .* n`existe pas",
            call_command,
            "servicetypes",
            "delete",
            "Abo Paris",
            stdout=None,
        )
        out = StringIO()
        call_command("servicetypes", "delete", "Abo Castre", stdout=out)
        result = out.getvalue()
        self.assertRegex(result, "supprimé")
        ServiceType.objects.get(name="VM")
        self.assertRaises(
            ServiceType.DoesNotExist,
            ServiceType.objects.get,
            name="Abo Castre",
        )


class ServiceLongStoppedTestCase(TestCase):
    def test_service_long_stopped(self):
        one_year_ago = datetime.now() - timedelta(days=365)
        two_years_ago = datetime.now() - timedelta(days=365 * 2)
        vm = ServiceType.objects.create(name="VM")
        User.objects.create_user(
            "admin",
            password="admin",
            is_staff=True,
            is_superuser=True,
        )
        old_user = User.objects.create(username="old user", password="old user")
        new_user = User.objects.create(username="new user", password="new user")
        old_contrib = RecurringPayment.objects.create()
        new_contrib = RecurringPayment.objects.create()
        PaymentUpdate.objects.create(
            payment=old_contrib,
            amount=0,
            period=1,
            payment_method=PaymentUpdate.STOP,
            start=two_years_ago,
            validated=True,
        )
        PaymentUpdate.objects.create(
            payment=new_contrib,
            amount=4.2,
            period=1,
            start=datetime.now(),
            validated=True,
        )
        old_service = Service.objects.create(
            adhesion=old_user.adhesion,
            service_type=vm,
            contribution=old_contrib,
            label="old service",
        )
        Service.objects.create(
            adhesion=new_user.adhesion,
            service_type=vm,
            contribution=new_contrib,
            label="new service",
        )

        # checks old service should trigger the "long_stopped" annotation

        self.assertIsNotNone(old_service.contribution)
        self.assertTrue(
            RecurringPayment.objects.filter(
                service=old_service,
                long_stopped=True,
            ).exists(),
        )
        self.assertFalse(
            ServiceAllocation.objects.filter(
                Q(service=old_service)
                & (Q(end__isnull=True) | Q(end__gte=one_year_ago)),
            ).exists(),
        )

        # everything else should be trivial now

        self.assertEqual(Service.objects.filter(long_stopped=True).count(), 1)
        self.assertEqual(Service.objects.filter(long_stopped=False).count(), 1)

        self.assertEqual(Service.objects.get(long_stopped=True).label, "old service")
        self.assertEqual(Service.objects.get(long_stopped=False).label, "new service")

        self.client.login(username="admin", password="admin")

        all_response = self.client.get("/admin/services/service/")
        old_response = self.client.get("/admin/services/service/", {"deletable": "1"})
        new_response = self.client.get("/admin/services/service/", {"deletable": "0"})

        self.assertEqual(all_response.status_code, 200)
        self.assertEqual(old_response.status_code, 200)
        self.assertEqual(new_response.status_code, 200)

        self.assertIn("old user", all_response.content.decode())
        self.assertIn("new user", all_response.content.decode())
        self.assertIn("old user", old_response.content.decode())
        self.assertIn("new user", new_response.content.decode())
        self.assertNotIn("new user", old_response.content.decode())
        self.assertNotIn("old user", new_response.content.decode())
