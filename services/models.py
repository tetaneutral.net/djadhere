from base64 import urlsafe_b64encode
from datetime import timedelta
from hashlib import sha256
from ipaddress import IPv4Address, IPv6Address, ip_network
from urllib.parse import quote

from django.apps import apps
from django.conf import settings
from django.core.exceptions import ValidationError
from django.core.validators import MaxValueValidator, MinValueValidator
from django.db import models
from django.db.models import Q
from django.urls import reverse
from django.utils import timezone
from django.utils.html import mark_safe

from banking.models import RecurringPayment
from djadhere.utils import get_active_filter, is_overlapping

from .utils.ip_conversion import ipv4_to_ipv6, ipv6_to_ipv4


def ipprefix_validator(value):
    try:
        ip_network(value)
    except ValueError as err:
        msg = "%s n`est pas un préfixe valide" % value
        raise ValidationError(msg) from err


class IPPrefix(models.Model):
    prefix = models.CharField(
        max_length=128,
        verbose_name="Préfixe",
        validators=[ipprefix_validator],
        unique=True,
    )

    class Meta:
        ordering = ["prefix"]
        verbose_name = "Réseau"
        verbose_name_plural = "Réseaux"

    def __str__(self):
        return self.prefix


class IPResourceManager(models.Manager):
    def get_queryset(self):
        qs = super().get_queryset()
        # On rajoute une super annotation « in_use » pour savoir si l`IP est dispo ou
        # non :-)
        return qs.annotate(
            in_use=models.Exists(
                ServiceAllocation.objects.filter(
                    Q(resource=models.OuterRef("pk")) & get_active_filter(),
                ),
            ),
        )
        # Ouf, pas de duplication car l`IP ne peut être utilisé que par un seul service!


class ActiveAllocationManager(models.Manager):
    def get_queryset(self):
        qs = super().get_queryset()
        return qs.annotate(
            active=models.Case(
                models.When(get_active_filter(), then=True),
                default=False,
                output_field=models.BooleanField(),
            ),
        )


class ActiveServiceManager(models.Manager):
    def get_queryset(self):
        qs = super().get_queryset()
        one_year_ago = timezone.now() - timedelta(days=365)
        return qs.annotate(
            active=models.Exists(
                ServiceAllocation.objects.filter(
                    Q(service=models.OuterRef("pk")) & get_active_filter(),
                ),
            ),
            allocations_long_stopped=~models.Exists(
                ServiceAllocation.objects.filter(
                    Q(service=models.OuterRef("pk"))
                    & (Q(end__isnull=True) | Q(end__gte=one_year_ago)),
                ),
            ),
            contribution_long_stopped=models.Exists(
                RecurringPayment.objects.filter(
                    service=models.OuterRef("pk"),
                    long_stopped=True,
                ),
            ),
            long_stopped=models.Case(
                models.When(contribution__isnull=True, then=False),
                models.When(contribution_long_stopped=False, then=False),
                models.When(allocations_long_stopped=False, then=False),
                default=True,
                output_field=models.BooleanField(),
            ),
        )


class IPResource(models.Model):
    CATEGORY_PUBLIC = 0
    CATEGORY_ANTENNA = 1
    CATEGORIES = (
        (CATEGORY_PUBLIC, "IP Public"),
        (CATEGORY_ANTENNA, "IP Antenne"),
    )

    ip = models.GenericIPAddressField(verbose_name="IP", primary_key=True)
    prefixes = models.ManyToManyField(IPPrefix, verbose_name="préfixes")
    reserved = models.BooleanField(default=False, verbose_name="réservée")
    category = models.IntegerField(choices=CATEGORIES, verbose_name="catégorie")
    checkmk_label = models.CharField(max_length=128, blank=True, default="")
    last_state_old = models.ForeignKey(
        "IPResourceStateOld",
        on_delete=models.PROTECT,
        related_name="+",
        verbose_name="dernier état",
        null=True,
    )
    last_state = models.ForeignKey(
        "IPResourceState",
        null=True,
        on_delete=models.PROTECT,
        related_name="+",
        verbose_name="dernier état",
    )
    last_state_up = models.ForeignKey(
        "IPResourceState",
        null=True,
        on_delete=models.PROTECT,
        related_name="+",
        verbose_name="dernier état UP",
    )
    last_time_up = models.DateTimeField(
        null=True,
        blank=True,
        verbose_name="Dernière réponse au ping",
    )

    objects = IPResourceManager()

    @property
    def allocations(self):
        return self.service_allocations if self.category == self.CATEGORY_PUBLIC else []

    @property
    def checkmk_url(self):
        if self.checkmk_label:
            return mark_safe(
                settings.CHECK_MK_URL.format(host=quote(self.checkmk_label)),
            )
        return None

    def password(self):
        data = sha256((settings.MASTER_PASSWORD + self.ip).encode("utf-8")).digest()
        return urlsafe_b64encode(data).decode("utf-8")[:8]

    password.short_description = "Mot de passe"

    def v4(self):
        if "." in self.ip:
            return IPv4Address(self.ip)
        # TODO: v6 only ?
        return ipv6_to_ipv4(self.v6())

    def v6(self):
        if ":" in self.ip:
            return IPv6Address(self.ip)
        return ipv4_to_ipv6(self.v4())

    class Meta:
        ordering = ["ip"]
        verbose_name = "IP"
        verbose_name_plural = "IP"

    def __str__(self):
        return str(self.ip)


class IPResourceStateCheck(models.Model):
    date = models.DateTimeField(default=timezone.now)

    class Meta:
        ordering = ["date"]

    def __str__(self):
        return str(self.date)


class IPResourceState(models.Model):
    STATE_DOWN = 0
    STATE_UP = 1
    STATE_CHOICES = (
        (STATE_DOWN, "DOWN"),
        (STATE_UP, "UP"),
    )
    ip = models.ForeignKey(
        IPResource,
        on_delete=models.CASCADE,
        related_name="state_set",
    )
    state = models.IntegerField(choices=STATE_CHOICES)
    begin = models.ForeignKey(
        IPResourceStateCheck,
        on_delete=models.PROTECT,
        related_name="+",
    )
    end = models.ForeignKey(
        IPResourceStateCheck,
        on_delete=models.PROTECT,
        related_name="+",
    )

    def __str__(self):
        return self.get_state_display()


class IPResourceStateOld(models.Model):
    STATE_DOWN = 0
    STATE_UP = 1
    STATE_UNKNOWN = 2
    STATE_CHOICES = (
        (STATE_DOWN, "DOWN"),
        (STATE_UP, "UP"),
        (STATE_UNKNOWN, "Inconnu"),
    )
    ip = models.ForeignKey(
        IPResource,
        on_delete=models.CASCADE,
        related_name="old_state_set",
    )
    date = models.DateTimeField(default=timezone.now)
    state = models.IntegerField(choices=STATE_CHOICES)

    def __str__(self):
        return self.get_state_display()


class ServiceType(models.Model):
    name = models.CharField(max_length=64, verbose_name="Nom", unique=True)
    contact = models.CharField(
        max_length=64,
        verbose_name="Contact en cas de problème",
        blank=True,
        default="",
    )

    class Meta:
        ordering = ["name"]
        verbose_name = "type de service"
        verbose_name_plural = "types de service"

    def __str__(self):
        return self.name


class Service(models.Model):
    """
    En cas d`ajout de champs, penser à mettre à jour la méthode save_model de
    ServiceAdmin.
    """

    adhesion = models.ForeignKey(
        "adhesions.Adhesion",
        verbose_name="Adhérent·e",
        related_name="services",
        on_delete=models.CASCADE,
    )
    service_type = models.ForeignKey(
        ServiceType,
        related_name="services",
        verbose_name="Type de service",
        on_delete=models.PROTECT,
    )
    label = models.CharField(blank=True, default="", max_length=128)
    notes = models.TextField(blank=True, default="")
    created = models.DateTimeField(auto_now_add=True)
    loan_equipment = models.BooleanField(default=False, verbose_name="Matériel en prêt")

    # WireGuard
    wireguard_pubkey = models.CharField(
        max_length=64,
        verbose_name="Clé public WireGuard",
        blank=True,
        default="",
    )

    contribution = models.OneToOneField(RecurringPayment, on_delete=models.CASCADE)

    objects = ActiveServiceManager()

    def save(self, *args, **kwargs):
        if not hasattr(self, "contribution"):
            self.contribution = RecurringPayment.objects.create()
        super().save(*args, **kwargs)

    def clean(self):
        super().clean()
        # Vérification de l`unicité par type de service du label
        if self.label != "" and Service.objects.exclude(pk=self.pk).filter(
            service_type=self.service_type,
            label=self.label,
        ):
            msg = "Un service du même type existe déjà avec ce label."
            raise ValidationError(msg)

    def is_active(self):
        return any(allocation.is_active() for allocation in self.allocations.all())

    is_active.boolean = True
    is_active.short_description = "Actif"

    @property
    def active_allocations(self):
        return self.allocations.filter(get_active_filter())

    @property
    def inactive_allocations(self):
        return self.allocations.exclude(get_active_filter())

    def get_absolute_url(self):
        return reverse(
            f"admin:{self._meta.app_label}_{self._meta.model_name}_change",
            args=(self.pk,),
        )

    def __str__(self):
        s = "#%d %s" % (self.pk, self.service_type)
        if self.label:
            s += " " + self.label
        return s


class Route(models.Model):
    name = models.CharField(max_length=64, unique=True)

    class Meta:
        ordering = ["name"]

    def get_ip(self):
        allocations = self.allocations.filter(get_active_filter())
        return allocations.values_list("resource", flat=True)

    def get_adh(self):
        Adhesion = apps.get_model("adhesions", "Adhesion")
        allocations = self.allocations.filter(get_active_filter())
        return Adhesion.objects.filter(
            pk__in=allocations.values_list("service__adhesion", flat=True),
        )

    def get_tel(self):
        adhesions = self.get_adh()
        user_tel = filter(
            lambda x: x,
            adhesions.values_list("user__profile__phone_number", flat=True),
        )
        corp_tel = filter(
            lambda x: x,
            adhesions.values_list("corporation__phone_number", flat=True),
        )
        return set(user_tel) | set(corp_tel)

    def get_email(self):
        adhesions = self.get_adh()
        user_email = filter(
            lambda x: x,
            adhesions.values_list("user__email", flat=True),
        )
        corp_email = filter(
            lambda x: x,
            adhesions.values_list("corporation__email", flat=True),
        )
        return set(user_email) | set(corp_email)

    def __str__(self):
        return self.name


class Tunnel(Route):
    description = models.CharField(max_length=128, blank=True)
    created = models.DateTimeField(
        default=timezone.now,
        verbose_name="Date de création",
    )
    ended = models.DateTimeField(
        null=True,
        blank=True,
        verbose_name="Date de désactivation",
    )
    port = models.IntegerField(null=True, blank=True)
    local_ip = models.GenericIPAddressField(
        null=True,
        blank=True,
        verbose_name="IP locale",
    )
    remote_ip = models.GenericIPAddressField(
        null=True,
        blank=True,
        verbose_name="IP distante",
    )
    networks = models.ManyToManyField(IPPrefix, blank=True, verbose_name="Réseaux")
    notes = models.TextField(blank=True, default="")

    def clean(self):
        super().clean()
        if self.ended:
            # Vérification de la cohérence des champs created et ended
            if self.created > self.ended:
                raise ValidationError(
                    {
                        "ended": "La date de désactivation doit être postérieur "
                        "à la date de création du tunnel.",
                    },
                )
        elif self.port:
            # Vérification de l`unicité d`un tunnel actif avec un port donné
            if (
                Tunnel.objects.exclude(pk=self.pk)
                .filter(port=self.port, ended__isnull=True)
                .exists()
            ):
                raise ValidationError(
                    {"port": "Ce numéro de port est déjà utilisé par un autre tunnel."},
                )


class ServiceAllocation(models.Model):
    start = models.DateTimeField(
        verbose_name="Début de la période d`allocation",
        default=timezone.now,
    )
    end = models.DateTimeField(
        null=True,
        blank=True,
        verbose_name="Fin de la période d`allocation",
    )
    resource = models.ForeignKey(
        IPResource,
        verbose_name="Ressource",
        related_name="service_allocations",
        related_query_name="service_allocation",
        limit_choices_to={"category": 0},
        on_delete=models.CASCADE,
    )
    service = models.ForeignKey(
        Service,
        related_name="allocations",
        related_query_name="allocation",
        on_delete=models.CASCADE,
    )
    route = models.ForeignKey(
        Route,
        verbose_name="Route",
        related_name="allocations",
        related_query_name="allocation",
        on_delete=models.PROTECT,
    )

    objects = ActiveAllocationManager()

    def clean(self):
        super().clean()
        # Vérification de la cohérence des champs start et end
        if self.end and self.start > self.end:
            msg = (
                "La date de début de l`allocation doit être antérieure "
                "à la date de fin de l`allocation."
            )
            raise ValidationError(msg)
        if self.resource_id:
            if self.resource.reserved and (not self.end or self.end > timezone.now()):
                msg = "L`IP sélectionnée est réservée"
                raise ValidationError(msg)
            # Vérification de l`abscence de chevauchement de la période d`allocation
            allocations = type(self).objects.filter(resource__pk=self.resource.pk)
            if is_overlapping(self, allocations):
                msg = (
                    "La période d`allocation de cette ressource chevauche "
                    "avec une période d`allocation précédente."
                )
                raise ValidationError(msg)

    def is_active(self):
        now = timezone.now()
        return self.start < now and (self.end is None or self.end > now)

    class Meta:
        ordering = ["-start"]
        verbose_name = "allocation"
        verbose_name_plural = "allocations"

    def __str__(self):
        return str(self.resource)


class Switch(models.Model):
    name = models.CharField(max_length=64, verbose_name="Nom", unique=True)
    emplacement = models.CharField(max_length=100, default="", blank=True)
    first_port = models.IntegerField(
        validators=[MinValueValidator(0)],
        verbose_name="Premier port",
    )
    last_port = models.IntegerField(
        validators=[MaxValueValidator(64)],
        verbose_name="Dernier port",
    )
    notes = models.TextField(blank=True, default="")

    class Meta:
        ordering = ("name",)

    def __str__(self):
        return self.name


class Port(models.Model):
    switch = models.ForeignKey(Switch, related_name="ports", on_delete=models.CASCADE)
    service = models.ForeignKey(
        Service,
        null=True,
        blank=True,
        related_name="ports",
        on_delete=models.SET_NULL,
    )
    port = models.IntegerField(verbose_name="N° de port")
    reserved = models.BooleanField(default=False, verbose_name="réservé")
    notes = models.CharField(max_length=256, blank=True, default="")
    up = models.BooleanField(null=True)

    def clean(self):
        if self.reserved and self.service:
            msg = "Un port réservé ne peut avoir de service."
            raise ValidationError(msg)

    class Meta:
        unique_together = (
            "switch",
            "port",
        )
        ordering = (
            "switch",
            "port",
        )

    def __str__(self):
        return "%s #%d" % (self.switch, self.port)
