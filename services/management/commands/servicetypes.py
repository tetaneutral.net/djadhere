from django.core.management.base import BaseCommand, CommandError

from services.models import Service, ServiceType


class Command(BaseCommand):
    help = "Gestion des types de services"

    def add_arguments(self, parser):
        subparsers = parser.add_subparsers(dest="command")
        subparsers.required = True

        subparsers.add_parser("list", help="Lister les types de services")

        parser_add = subparsers.add_parser(
            "add",
            help="Ajouter un nouveau type de service",
        )
        parser_add.add_argument("name", help="Nom du nouveau type de service")

        parser_change = subparsers.add_parser(
            "change",
            help="Renomer un type de service",
        )
        parser_change.add_argument("old_name", help="Ancien nom")
        parser_change.add_argument("new_name", help="Nouveau nom")

        parser_delete = subparsers.add_parser(
            "delete",
            help="Supprimer un type de service",
        )
        parser_delete.add_argument("name", help="Nom du type de service")

    def handle(self, *args, **options):
        cmd = options.pop("command")
        getattr(self, f"handle_{cmd}")(*args, **options)

    def handle_list(self, *args, **options):
        for st in ServiceType.objects.all():
            self.stdout.write(f"{st.name:<30} {st.services.count():4d} services")

    def handle_add(self, *args, **options):
        if ServiceType.objects.filter(name=options["name"]).exists():
            raise CommandError('Le type de service "%s" existe déjà' % options["name"])
        st = ServiceType.objects.create(name=options["name"])
        self.stdout.write(
            self.style.SUCCESS(
                'Nouveau type de service "%s" créé avec succès' % st.name,
            ),
        )

    def handle_change(self, *args, **options):
        try:
            st = ServiceType.objects.get(name=options["old_name"])
        except ServiceType.DoesNotExist as err:
            msg = 'Le type de service "%s" n`existe pas' % options["old_name"]
            raise CommandError(msg) from err
        if ServiceType.objects.filter(name=options["new_name"]).exists():
            raise CommandError(
                'Le type de service "%s" existe déjà' % options["new_name"],
            )
        st.name = options["new_name"]
        st.save()
        self.stdout.write(
            self.style.SUCCESS('Type de service renommé en "%s"' % options["new_name"]),
        )

    def handle_delete(self, *args, **options):
        try:
            st = ServiceType.objects.get(name=options["name"])
        except ServiceType.DoesNotExist as err:
            msg = 'Le type de service "%s" n`existe pas' % options["name"]
            raise CommandError(msg) from err
        if Service.objects.filter(service_type=st).exists():
            raise CommandError(
                'Il existe des services de type "%s", suppression refusé'
                % options["name"],
            )
        st.delete()
        self.stdout.write(
            self.style.SUCCESS(
                'Le type de service "%s" a été supprimé' % options["name"],
            ),
        )
