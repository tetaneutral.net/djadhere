import argparse
import logging
from datetime import datetime, timedelta
from itertools import groupby

from django.conf import settings
from django.contrib.humanize.templatetags.humanize import naturaltime
from django.core.management.base import BaseCommand
from django.utils import timezone

from djadhere.utils import get_active_filter, send_web_notification
from services.models import IPResource, IPResourceState, ServiceAllocation

logger = logging.getLogger(__name__)


def datehourmin(s):
    try:
        return timezone.make_aware(datetime.strptime(s, "%Y-%m-%d %H:%M"))
    except ValueError as err:
        msg = f"Not a valid date: '{s}'."
        raise argparse.ArgumentTypeError(msg) from err


class Command(BaseCommand):
    help = "Détection des pannes"

    def add_arguments(self, parser):
        parser.add_argument(
            "-d",
            "--date",
            help=r"Chercher une panne à une date donnée (%Y-%m-%d %H:%M).",
            type=datehourmin,
            default=timezone.now(),
        )
        parser.add_argument("--before", type=int)  # , default=5)
        parser.add_argument("--after", type=int)  # , default=0)
        parser.add_argument("--threshold", type=int)
        parser.add_argument("--verbose", action="store_true")

    def handle(self, *args, date, before, after, threshold, verbose, **options):
        down = IPResource.objects.filter(
            last_state__state=IPResourceState.STATE_DOWN,
            last_state_up__isnull=False,
        )
        allocations = (
            ServiceAllocation.objects.filter(get_active_filter())
            .filter(resource__ip__in=down.values("ip"))
            .select_related(
                "route",
                "resource__last_state",
                "resource__last_state__begin",
            )
        )
        if before:
            allocations = allocations.filter(
                resource__last_state__begin__date__lte=date + timedelta(minutes=before),
            )
        if after:
            allocations = allocations.filter(
                resource__last_state__begin__date__gte=date + timedelta(minutes=after),
            )
        out = []
        if not threshold:
            threshold = getattr(settings, "STATUS_THRESHOLD", 2)
        allocations = sorted(
            allocations,
            reverse=True,
            key=lambda a: (a.resource.last_state.begin.date, a.route.pk),
        )
        for (begin_down, route), allocs in groupby(
            allocations,
            key=lambda a: (a.resource.last_state.begin.date, a.route.pk),
        ):
            allocs = list(allocs)
            route = allocs[0].route
            if verbose:
                out.append("%s: %d" % (route, len(allocs)))
                for alloc in allocs:
                    out.append("\t%s" % alloc.resource)
            if len(allocs) >= threshold:
                out.append(
                    "Coupure corrélée de {} IPs sur {} {} :".format(
                        len(allocs),
                        route,
                        naturaltime(begin_down),
                    ),
                )
                for alloc in allocs:
                    out.append(f"\t- {alloc.resource}")
        if out:
            for line in out:
                self.stdout.write(line)
            send_web_notification("epicfail:", "\n```\n" + "\n".join(out) + "\n```")
