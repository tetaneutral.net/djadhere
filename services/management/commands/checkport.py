from django.core.management.base import BaseCommand

from services.models import Switch


class Command(BaseCommand):
    help = "Analyse de l`état des ports sur les switchs et dans Djadhere."

    def handle(self, *args, **options):
        for sw in Switch.objects.all():
            for port in sw.ports.all():
                if port.up is True:
                    if port.service:
                        continue
                    if port.reserved:
                        continue
                    print(f"{port} UP - {port.notes}")
                elif port.up is False:
                    if port.reserved:
                        continue
                    if port.service:
                        print(f"{port} DOWN - {port.service}")
                        continue
                    if port.notes:
                        print(f"{port} DOWN - {port.notes}")
                        continue
                else:
                    if port.notes:
                        print(f"{port} UNKNOWN - {port.notes}")

                # self.stdout.write(
                # self.style.ERROR(
                # "Impossible de parser la ligne n°%d: '%s'" % (i, line[:1])
                # )
                # )
                # continue
            # resource = IPResource.objects.get(ip=g.group(1))
            # route = g.group(2)
            # if route == "reserved":
            # if resource.in_use:
            # alloc = resource.allocations.get(active=True)
            # self.stdout.write(
            # self.style.WARNING(
            # (
            # "L`IP %s est marqué réservée dans ip_ttnn.txt mais "
            # "allouée au service #%d sur Djadhere"
            # )
            # % (resource, alloc.service.pk)
            # )
            # )
            # if not resource.reserved:
            # self.stdout.write(
            # self.style.WARNING(
            # (
            # "L`IP %s est marqué réservée dans ip_ttnn.txt "
            # "mais pas dans Djadhere"
            # )
            # % resource
            # )
            # )
            # continue
            # if route == "unused":
            # if resource.in_use:
            # alloc = resource.allocations.get(active=True)
            # self.stdout.write(
            # self.style.WARNING(
            # (
            # "L`IP %s est marqué disponible dans ip_ttnn.txt "
            # "mais allouée au service #%d sur Djadhere"
            # )
            # % (resource, alloc.service.pk)
            # )
            # )
            # continue
            # try:
            # route = Route.objects.get(name=route)
            # except Route.DoesNotExist:
            # self.stdout.write(
            # self.style.WARNING(
            # (
            # "L`IP %s est routée sur %s dans ip_ttnn.txt "
            # "mais cette route est inconnue dans Djadhere"
            # )
            # % (resource, route)
            # )
            # )
            # continue
            # if resource.in_use:
            # alloc = resource.allocations.get(active=True)
            # if alloc.route != route:
            # self.stdout.write(
            # self.style.WARNING(
            # (
            # "L`IP %s est routée sur %s dans ip_ttnn.txt "
            # "mais routée sur %s pour le service #%d sur Djadhere"
            # )
            # % (resource, route, alloc.route, alloc.service.pk)
            # )
            # )
            # continue
            # if resource.reserved:
            # self.stdout.write(
            # self.style.WARNING(
            # (
            # "L`IP %s est marqué réservée dans Djadhere "
            # "alors que routée via %s dans ip_ttnn.txt"
            # )
            # % (resource, route)
            # )
            # )
            # if check_reverse:
            # try:
            # reverse, _, _ = socket.gethostbyaddr(str(resource))
            # except:
            # reverse = "?"
            # else:
            # reverse = "?"
            # self.stdout.write(
            # self.style.WARNING(
            # (
            # "L`IP %s est disponible dans Djadhere "
            # "alors que routée via %s dans ip_ttnn.txt (reverse : %s)"
            # )
            # % (resource, route, reverse)
            # )
            # )
