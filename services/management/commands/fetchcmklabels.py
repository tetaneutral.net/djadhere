from itertools import groupby

from django.core.management.base import BaseCommand

from djadhere.utils import from_livestatus
from services.models import IPResource


class Command(BaseCommand):
    help = "Récupération du label check_mk"

    def handle(self, *args, **options):
        hosts = from_livestatus("hosts", columns=["name", "address"])
        # quelques IP sont listées curieusement plusieurs fois, on prend la première
        # occurence
        for address, group in groupby(
            sorted(hosts, key=lambda host: host.address),
            lambda host: host.address,
        ):
            host = next(group)
            IPResource.objects.filter(ip=address).update(checkmk_label=host.name)
