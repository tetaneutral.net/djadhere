from django.core.management.base import BaseCommand, CommandError

from services.models import Route


class Command(BaseCommand):
    help = "Informations sur les routes"

    def add_arguments(self, parser):
        parser.add_argument("route")
        group = parser.add_mutually_exclusive_group(required=True)
        group.add_argument("--ip", action="store_true")
        group.add_argument("--adh", action="store_true")
        group.add_argument("--tel", action="store_true")
        group.add_argument("--email", action="store_true")

    def handle(self, *args, **options):
        try:
            route = Route.objects.get(name=options["route"])
        except Route.DoesNotExist as err:
            msg = "La route « %s » n`existe pas." % options["route"]
            raise CommandError(msg) from err
        if options["ip"]:
            for ip in route.get_ip():
                print(ip)
        elif options["adh"]:
            for adh in route.get_adh():
                print(f"{adh} ({adh.adherent})")
        elif options["tel"]:
            for tel in route.get_tel():
                print(tel)
        elif options["email"]:
            for tel in route.get_email():
                print(tel)
