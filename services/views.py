from itertools import groupby

from django.conf import settings
from django.contrib.auth.mixins import LoginRequiredMixin
from django.db import models
from django.db.models.functions import Concat
from django.http import HttpResponse, HttpResponseForbidden, JsonResponse
from django.shortcuts import get_object_or_404
from django.views.decorators.csrf import csrf_exempt
from django.views.decorators.http import require_GET, require_POST
from django.views.generic import DetailView, TemplateView

from djadhere.decorators import api_key_required
from djadhere.utils import get_active_filter

from .models import IPResource, IPResourceState, Route, Service, ServiceAllocation
from .utils.architecture import export_ip, export_switch
from .utils.fastpinger import fastpinger_update


class Status(LoginRequiredMixin, TemplateView):
    template_name = "services/status.html"

    def get_status(self):
        down = IPResource.objects.filter(
            last_state__state=IPResourceState.STATE_DOWN,
            last_state_up__isnull=False,
        )
        allocations = (
            ServiceAllocation.objects.filter(get_active_filter())
            .filter(resource__ip__in=down.values("ip"))
            .select_related(
                "route",
                "resource__last_state",
                "resource__last_state__begin",
            )
        )
        events = []
        threshold = getattr(settings, "STATUS_THRESHOLD", 2)
        allocations = sorted(
            allocations,
            reverse=True,
            key=lambda a: (a.resource.last_state.begin.date, a.route.pk),
        )
        for (begin_down, route), allocs in groupby(
            allocations,
            key=lambda a: (a.resource.last_state.begin.date, a.route.pk),
        ):
            allocs = list(allocs)
            route = allocs[0].route
            if len(allocs) >= threshold:
                events.append((route, begin_down, allocs))
        return events

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["status"] = self.get_status()
        return context


class ServiceDetail(LoginRequiredMixin, DetailView):
    def get_queryset(self):
        if self.request.user.is_superuser:
            return Service.objects
        adhesions = self.request.user.profile.adhesions.values_list("pk")
        return Service.objects.filter(adhesion__pk__in=adhesions).order_by(
            "service_type",
        )


@csrf_exempt
@require_POST
def fastpinger(request):
    if request.POST.get("key", None) != settings.FASTPINGER_KEY:
        return HttpResponseForbidden("Invalid key.")
    if "fastpinger" not in request.FILES:
        return HttpResponse(status=400)  # Bad Request
    stats = fastpinger_update(request.FILES["fastpinger"])
    return HttpResponse(stats + "\n")


@require_GET
@api_key_required
def routes(request, route=None):
    resources = IPResource.objects.filter(category=IPResource.CATEGORY_PUBLIC)
    if route:
        route = get_object_or_404(Route, name=route)
        resources = resources.filter(pk__in=route.get_ip())
    resources = resources.annotate(
        route=models.Subquery(
            ServiceAllocation.objects.filter(
                models.Q(resource=models.OuterRef("pk")) & get_active_filter(),
            ).values("route__name")[:1],
        ),
    )
    resources = resources.annotate(
        route=models.Case(
            models.When(reserved=True, then=models.Value("reserved")),
            models.When(route__isnull=True, then=models.Value("unused")),
            default="route",
            output_field=models.CharField(),
        ),
    )
    resources = resources.annotate(
        route=Concat("ip", models.Value(" "), "route", output_field=models.CharField()),
    )
    resources = resources.annotate(
        order=models.Case(
            models.When(ip__startswith="91.224", then=1),
            models.When(ip__startswith="89.234", then=2),
            models.When(ip__startswith="185.119", then=3),
        ),
    )
    resources = resources.order_by("order", "ip")
    content = "\n".join(resources.values_list("route", flat=True)) + "\n"
    return HttpResponse(content, content_type="text/plain; charset=utf-8")


@require_GET
@api_key_required
def architecture_switch(request):
    return HttpResponse(export_switch(), content_type="text/plain; charset=utf-8")


@require_GET
@api_key_required
def architecture_ip(request):
    return HttpResponse(export_ip(), content_type="text/plain; charset=utf-8")


@require_GET
@api_key_required
def wireguard(request):
    allocations = ServiceAllocation.objects.filter(
        get_active_filter() & models.Q(service__service_type__name="wireguard"),
    ).annotate(
        wireguard=Concat(
            "resource__ip",
            models.Value(" "),
            "service__wireguard_pubkey",
            output_field=models.CharField(),
        ),
    )
    content = "\n".join(allocations.values_list("wireguard", flat=True))
    return HttpResponse(content, content_type="text/plain; charset=utf-8")


@csrf_exempt
@require_POST
@api_key_required
def vm(request, pk):
    service = get_object_or_404(Service, pk=pk, service_type__name="VM")
    if service.allocations.count() != 1:
        return HttpResponse(
            "Cette API ne marche que pour les VM avec une et une seule IP. "
            f"Là il y en a {service.allocations.count()=}",
            status=400,
        )
    allocation = service.allocations.first()
    route = allocation.route.name
    if not route.startswith("vlan-"):
        return HttpResponse(
            "Cette API ne marche que pour les VM routées sur vlan-xxx. "
            f"Là, c’est {route=}",
            status=400,
        )
    return JsonResponse(
        {
            "label": service.label,
            "ipv4": str(allocation.resource.v4()),
            "ipv6": str(allocation.resource.v6()),
            "vlan": int(route.removeprefix("vlan-")),
            "ssh": service.adhesion.get_ssh_keys(),
        },
    )


@csrf_exempt
@require_POST
@api_key_required
def vlans(request):
    ret = []
    for r in Route.objects.filter(name__startswith="vlan-", allocation__isnull=False):
        v = r.name.removeprefix("vlan-")
        ret.append(f"# VLAN {v} {r.allocations.first().service.adhesion}")
        ret.append(f"ip link add link bond0 name bond0.{v} type vlan id {v}")
        ret.append(f"ip link set bond0.{v} up")
        ret.append(f"ip -6 addr add fe80::31/64 dev bond0.{v}")
        for allocation in r.allocations.all():
            ret.append(f"## service {allocation.service}")
            v4 = allocation.resource.v4()
            ret.append(f"ip route add {v4} dev bond0.{v}")
            v6 = allocation.resource.v6()
            x6 = str(v6).removeprefix("2a03:7220:80").removesuffix("00::/56")
            ret.append(f"ip -6 route add {v6} via fe80::{x6} dev bond0.{v}")
        ret.append("")
    return HttpResponse("\n".join(ret))
