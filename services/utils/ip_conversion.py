from ipaddress import IPv4Address, IPv4Network, IPv6Address, IPv6Network

IPV4_NETWORKS = [
    IPv4Network("91.224.148.0/24"),
    IPv4Network("91.224.149.0/24"),
    IPv4Network("89.234.156.0/24"),
    IPv4Network("89.234.157.0/24"),
    IPv4Network("185.119.168.0/24"),
    IPv4Network("185.119.169.0/24"),
    IPv4Network("185.119.170.0/24"),
    IPv4Network("185.119.171.0/24"),
]

IPV6_NETWORKS = [
    IPv6Network("2a03:7220:8080::/48"),
    IPv6Network("2a03:7220:8081::/48"),
    IPv6Network("2a03:7220:8083::/48"),
    IPv6Network("2a03:7220:8084::/48"),
    IPv6Network("2a03:7220:8085::/48"),
    IPv6Network("2a03:7220:8086::/48"),
    IPv6Network("2a03:7220:8087::/48"),
    IPv6Network("2a03:7220:8088::/48"),
]


def ipv4_to_ipv6(ipv4):
    ipv4_network = IPv4Network((ipv4, 24), strict=False)
    ipv6_network = IPV6_NETWORKS[IPV4_NETWORKS.index(ipv4_network)]
    return list(ipv6_network.subnets(8))[ipv4.packed[3]]


def ipv6_to_ipv4(ipv6):
    ipv6_network = IPv6Network((ipv6, 48), strict=False)
    ipv4_network = IPV4_NETWORKS[IPV6_NETWORKS.index(ipv6_network)]
    return ipv4_network[ipv6.packed[6]]


if __name__ == "__main__":
    ipv4_to_ipv6_tests = {
        IPv4Address("91.224.148.0"): IPv6Network("2a03:7220:8080::/56"),
        IPv4Address("91.224.149.170"): IPv6Network("2a03:7220:8081:aa00::/56"),
        IPv4Address("89.234.156.195"): IPv6Network("2a03:7220:8083:c300::/56"),
        IPv4Address("89.234.157.255"): IPv6Network("2a03:7220:8084:ff00::/56"),
    }
    for ipv4, ipv6 in ipv4_to_ipv6_tests.items():
        if ipv4_to_ipv6(ipv4) != ipv6:
            msg = f"{ipv4_to_ipv6(ipv4)=} ≠ {ipv6=}"
            raise AssertionError(msg)

    ipv6_to_ipv4_tests = {
        IPv6Address("2a03:7220:8080:ff0d:35ac:3820::23"): IPv4Address("91.224.148.255"),
        IPv6Address("2a03:7220:8081:aa0d:35ac:3820::23"): IPv4Address("91.224.149.170"),
        IPv6Address("2a03:7220:8083:000d:35ac:3820::0"): IPv4Address("89.234.156.0"),
        IPv6Address("2a03:7220:8084:220d:35ac:3820::1"): IPv4Address("89.234.157.34"),
        IPv6Address("2a03:7220:8085:800d:35ac:3820::ff"): IPv4Address(
            "185.119.168.128",
        ),
        IPv6Address("2a03:7220:8086:2a0d:35ac:3820::f"): IPv4Address("185.119.169.42"),
        IPv6Address("2a03:7220:8087:f00d:35ac:3820::a"): IPv4Address("185.119.170.240"),
        IPv6Address("2a03:7220:8088:f59d:35ac:abcd:dead:beaf"): IPv4Address(
            "185.119.171.245",
        ),
    }
    for ipv6, ipv4 in ipv6_to_ipv4_tests.items():
        if ipv6_to_ipv4(ipv6) != ipv4:
            msg = f"{ipv6_to_ipv4(ipv6)=} ≠ {ipv4=}"
            raise AssertionError(msg)
