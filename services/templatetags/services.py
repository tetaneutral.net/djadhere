from django import template

from djadhere.utils import get_active_filter

register = template.Library()


@register.filter
def active(services):
    return services.filter(get_active_filter("allocation")).distinct()


@register.filter
def inactive(services):
    return services.exclude(get_active_filter("allocation")).distinct()


@register.inclusion_tag("services/tags/deallocate_modal_div.html")
def deallocate_modal_div():
    return {}


@register.inclusion_tag("services/tags/deallocate_modal_js.html")
def deallocate_modal_js():
    return {}
