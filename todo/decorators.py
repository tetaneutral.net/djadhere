from functools import wraps

from django.contrib.auth.decorators import login_required
from django.core.exceptions import PermissionDenied
from django.shortcuts import get_object_or_404

from .models import TaskList


def allowed_tasklist_required(view_func):
    def wrapped_view(request, *args, **kwargs):
        if not request.user.is_authenticated:
            return login_required(view_func)(request, *args, **kwargs)
        tasklist_slug = kwargs.pop("tasklist_slug")
        tasklist = get_object_or_404(TaskList, slug=tasklist_slug)
        if not request.user.is_superuser and not request.user.groups.all().intersection(
            tasklist.groups.all(),
        ):
            raise PermissionDenied
        kwargs["tasklist"] = tasklist
        return view_func(request, **kwargs)

    return wraps(view_func)(wrapped_view)
