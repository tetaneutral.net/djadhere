from django.conf import settings
from django.contrib import admin
from django.contrib.admin.views.autocomplete import AutocompleteJsonView
from django.core.exceptions import PermissionDenied
from django.http import JsonResponse

# setup autocomplete with custom label
# thx https://stackoverflow.com/a/56865950/1368502


class CustomAutocompleteJsonView(AutocompleteJsonView):
    def get(self, request, *args, **kwargs):
        from services.admin import RouteAdmin

        (
            self.term,
            self.model_admin,
            self.source_field,
            to_field_name,
        ) = self.process_request(request)

        if not self.has_perm(request):
            raise PermissionDenied

        self.object_list = self.get_queryset()
        context = self.get_context_data()
        if isinstance(self.model_admin, RouteAdmin):
            return JsonResponse(
                {
                    "results": [
                        {
                            "id": str(route.pk),
                            "text": f"{route} ({route.get_ip().count()} ip)",
                        }
                        for route in context["object_list"]
                    ],
                    "pagination": {"more": context["page_obj"].has_next()},
                },
            )
        return super().get(request, *args, **kwargs)


class DjadhereAdminSite(admin.AdminSite):
    site_title = settings.SITE_TITLE
    site_header = settings.SITE_HEADER

    def autocomplete_view(self, request):
        return CustomAutocompleteJsonView.as_view(admin_site=self)(request)


# admin_site = DjadhereAdminSite()
