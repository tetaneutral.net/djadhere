from django.apps import AppConfig
from django.contrib.admin import apps


class DjadminConfig(AppConfig):
    default_auto_field = "django.db.models.BigAutoField"
    name = "djadmin"


class DjadhereAdminConfig(apps.AdminConfig):
    # label = "djadhere_admin"
    default_site = "djadmin.admin.DjadhereAdminSite"
