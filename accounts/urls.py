from django.contrib.auth import views as auth_views
from django.urls import include, path, re_path

from . import views
from .forms import PasswordResetForm

urlpatterns = [
    path("profile/", views.profile, name="profile"),
    re_path(r"^auth_api/(?P<token>[a-zA-Z0-9]{32})/$", views.auth_api, name="auth_api"),
    path(
        "password_reset/",
        auth_views.PasswordResetView.as_view(form_class=PasswordResetForm),
        name="password_reset",
    ),
    path("login/", views.login, name="login"),
    path("logout/", views.logout, name="logout"),
    path("", include("django.contrib.auth.urls")),
]
