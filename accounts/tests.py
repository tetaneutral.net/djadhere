from django.core import mail
from django.test import TestCase
from django.urls import reverse

from adhesions.models import User

from .forms import ProfileForm, UserForm


class ViewsTestCase(TestCase):
    def setUp(self):
        User.objects.create_user(
            "user",
            first_name="first",
            last_name="last",
            email="user@example.net",
            password="user",
        )

    def test_auth(self):
        self.assertEqual(self.client.get(reverse("login")).status_code, 200)
        self.assertEqual(self.client.get(reverse("password_reset")).status_code, 200)
        self.assertEqual(
            self.client.get(reverse("password_reset_done")).status_code,
            200,
        )

    def test_email_backend(self):
        self.assertFalse(
            self.client.login(username="user@example.net", password="wrong"),
        )
        self.assertFalse(
            self.client.login(username="wrong@example.net", password="user"),
        )
        self.assertTrue(self.client.login(username="user@example.net", password="user"))

    def test_login_logout(self):
        self.assertEqual(self.client.get(reverse("login")).status_code, 200)
        self.client.login(username="user", password="user")
        self.assertEqual(self.client.get(reverse("adhesion-detail")).status_code, 200)
        self.assertRedirects(
            self.client.get(reverse("logout")),
            reverse("adhesion-detail"),
            target_status_code=302,
        )  # user page is redirected to login
        self.assertRedirects(
            self.client.get(reverse("adhesion-detail")),
            reverse("login") + "?next=" + reverse("adhesion-detail"),
        )

    def test_profile(self):
        response = self.client.get(reverse("profile"))
        self.assertRedirects(response, reverse("login") + "?next=" + reverse("profile"))
        self.client.login(username="user", password="user")
        response = self.client.get(reverse("profile"))
        self.assertEqual(response.status_code, 200)
        user = User.objects.get(username="user")
        user_form = UserForm(None, instance=user)
        data = {key: getattr(user_form.instance, key) for key in user_form.fields}
        profile_form = ProfileForm(instance=user.profile)
        data.update(
            {key: getattr(profile_form.instance, key) for key in profile_form.fields},
        )
        data["username"] = "user2"  # try to tamper username
        data["first_name"] = "first2"  # try to tamper username
        data["last_name"] = "last2"  # try to tamper username
        data["email"] = "user@example.org"
        data["address"] = "221B Baker Street"
        response = self.client.post(reverse("profile"), data)
        self.assertRedirects(response, reverse("profile"))
        user = User.objects.get(pk=user.pk)  # refresh user
        self.assertEqual(user.username, "user")  # should not be modified
        self.assertEqual(user.first_name, "first")  # should not be modified
        self.assertEqual(user.last_name, "last")  # should not be modified
        self.assertEqual(user.email, "user@example.org")
        self.assertEqual(user.profile.address, "221B Baker Street")

    def test_password_reset(self):
        response = self.client.get(reverse("password_reset"))
        self.assertEqual(response.status_code, 200)
        for addr, mails in (("bad@aol.com", 0), ("user@example.net", 1)):
            response = self.client.post(reverse("password_reset"), {"email": addr})
            self.assertEqual(len(mail.outbox), mails)
