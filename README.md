# Djadhere

Système d’Information (SI) du FAI associatif tetaneutral.net

* Code source : https://code.ffdn.org/tetaneutral.net/djadhere/
* Bugs : https://code.ffdn.org/tetaneutral.net/djadhere/issues/
* Wiki : https://chiliproject.tetaneutral.net/projects/tetaneutral/wiki/Djadhere/
* Jabber : djadhere@chat.cannelle.eu.org ([webchat](https://jappix.cannelle.eu.org/?r=djadhere@chat.cannelle.eu.org))
* IRC : `#djadhere` sur le réseau `irc.freenode.net` ([webchat](https://kiwiirc.com/client/chat.freenode.net?chan=#djadhere))


## Local test

```
export UID=$(id -u)
export GID=$(id -g)
docker compose down -v && docker compose up --build --exit-code-from tests
```
